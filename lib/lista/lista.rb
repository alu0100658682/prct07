class Biblio
   attr_reader :autor,:titulo,:serie,:editorial,:numero_edicion,:fecha_publicacion,:numero_ISBN
  
  def initialize(aut, titulo, serie, editorial, edicion, publicacion, isbn)
  
		@autor = Array.new(aut)
		@titulo = titulo
		@serie = serie
		@editorial = editorial
		@numero_edicion = edicion
		@fecha_publicacion = publicacion
		@numero_ISBN = Array.new(isbn)
    
  end
 
  def to_s
    
    "#{@autor}, #{@titulo}, #{@serie}, #{@editorial}, #{@numero_edicion}, #{@fecha_publicacion}, #{@numero_ISBN}"
	
  end
  
end


Node = Struct.new(:value, :next)

class List
    
    attr_accessor :cabeza

    def initialize(cabeza)
        
        @cabeza = cabeza
        
    end
    
    def listavacia()
        
        if (@cabeza == nil)
            true
        elsif (@cabeza != nil)
            false
        end
        
    end
    
    def insert(nodo)
        
        if (@cabeza == nil)
            
            @cabeza = nodo
                
        elsif (@cabeza != nil)
        
            nodo.next = @cabeza
            @cabeza = nodo
                
        end
    end
    
    def insert2(array)
        
        for i in 0..(array.length-1)
        
            insert(array[i])
            
        end
    end
    
    def nodelist()
        
        node = @cabeza 
        a = String.new()
        
        loop do
            
            a << node.value.to_s
            node = node.next
        
        break if (node == nil)
        end
        return  a
    end
    
    def deletelast
        
        node = @cabeza.next
        if (node != nil)
            
            node.next = nil
            @cabeza = node
            
        elsif (node == nil)
        
            @cabeza = nil
            
        end
        
    end
    
    def extraerprimero()
        nodo = @cabeza
        
        while (nodo.next != nil)
            
            nodo = nodo.next
            
        end
        
        return nodo
    end

end